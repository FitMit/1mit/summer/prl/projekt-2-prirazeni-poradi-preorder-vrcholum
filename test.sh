#!/bin/bash

# test.sh
# Preorder using Euler tour, PRL project 2, 24.4.2022
# Author: Matěj Kudera, xkuder04, VUT FIT
# Test script which runs implementation o parallel preorder

# Get argument with binary tree
if [ $# -eq 1 ]
then
    # Calculate number of needed processors
    lenght=${#1}
    cpu=$((($lenght * 2) - 2))
    
    # If binary tree contains only root, then needed number of processors is 1
    if [ $cpu -eq 0 ]
    then
        cpu=1
    fi

    # Compile source file
    mpic++ --prefix /usr/local/share/OpenMPI -o pro pro.cpp

    # Run compiled program with needed arguments
    mpirun --prefix /usr/local/share/OpenMPI -oversubscribe -np $cpu pro "$1"

    # TODO Remove program? ¯\_(ツ)_/¯
    # rm -f pro
else
    # TODO return nothing for automated control? ¯\_(ツ)_/¯
    echo "Test script is used in format:"
    echo ""
    echo "bash test.sh input_binary_tree"
    echo ""
    echo "- Binary tree is stored in string which contains its vertices. Vertices are numbered starting from 1 and every vertex is represented exactly by one character in string. Left child of vertex 'i' is stored on position '2*i' and right child on position '(2*i)+1'."
fi

# END test.sh
