// pro.cpp
// Preorder using Euler tour, PRL project 2, 24.4.2022
// Author: Matěj Kudera, xkuder04, VUT FIT
// Main file with implementation of parallel preorder traversal in binary tree

// ########################### Includes ###########################
#include <mpi.h>

#include <iostream>
#include <string>

#include <math.h>

using namespace std;

// ########################### Preprocesor directives ###########################
#define DEFAULT_TAG 1
#define DEBUG false

// ########################### Data structures ###########################
struct adjacency_list_element
{
	int start_vertex;
	int end_vertex;
   	int reverse_edge_pos;
   	int next_edge_pos;
}; 

// ########################### Functions ###########################

// Make adjacency list for binary tree
void make_adjacency_list(int num_of_vertices, int num_of_edges, int *vertex_start_index, struct adjacency_list_element *adjacency_list)
{
	// Go through vertices and make structure for every edge
	// Map vertices to unique int value
	int edge_index = 0;
	for (int i = 0; i < num_of_vertices; i++)
	{
		// Get start position in edge array
		vertex_start_index[i] = edge_index;

		// Check left offspring
		bool offspring_added = false;
		int left_offspring_index = 2*i+1;
		if (left_offspring_index < num_of_vertices)
		{
			adjacency_list[edge_index].start_vertex = i;
			adjacency_list[edge_index].end_vertex = left_offspring_index;			
			adjacency_list[edge_index].reverse_edge_pos = -1;
			adjacency_list[edge_index].next_edge_pos = -1;

			edge_index++;
			offspring_added = true;
		}

		// Check right offspring
		int right_offspring_index = 2*i+2;
		if (right_offspring_index < num_of_vertices)
		{
			adjacency_list[edge_index].start_vertex = i;
			adjacency_list[edge_index].end_vertex = right_offspring_index;			
			adjacency_list[edge_index].reverse_edge_pos = -1;
			adjacency_list[edge_index].next_edge_pos = -1;

			// Add next edge position to last added edge
			adjacency_list[edge_index-1].next_edge_pos = edge_index;

			edge_index++;
			offspring_added = true;
		}

		// Calculate index to parent of vertex
		float parent_index_L = (i-1)/2.0;
		float parent_index_R = (i-2)/2.0;

		// Valid parent index is whole number
		if ((ceilf(parent_index_L) == parent_index_L) and (parent_index_L >= 0))
		{
			// Get interval with edges of parent vertex
			int parent_index = int(parent_index_L);
			int last_edge_index;
			int reverse_edge_index;

			if (parent_index+1 < num_of_vertices)
			{
				last_edge_index = vertex_start_index[parent_index+1];
			}
			else
			{
				last_edge_index = num_of_vertices;
			}

			for (int j = parent_index; j < last_edge_index; j++)
			{
				if (adjacency_list[j].end_vertex == i)
				{
					reverse_edge_index = j;
					adjacency_list[j].reverse_edge_pos = edge_index;	
					break;
				}

				// Reverse edge will be allways found
			}			

			adjacency_list[edge_index].start_vertex = i;
			adjacency_list[edge_index].end_vertex = parent_index;			
			adjacency_list[edge_index].reverse_edge_pos = reverse_edge_index;
			adjacency_list[edge_index].next_edge_pos = -1;

			// Add next edge position to last added edge
			if (offspring_added)
			{
				adjacency_list[edge_index-1].next_edge_pos = edge_index;
			}

			edge_index++;
		}
		else if ((ceilf(parent_index_R) == parent_index_R) and (parent_index_R >= 0))
		{
			// Get interval with edges of parent vertex
			int parent_index = int(parent_index_R);
			int last_edge_index;
			int reverse_edge_index;

			if (parent_index+1 < num_of_vertices)
			{
				last_edge_index = vertex_start_index[parent_index+1];
			}
			else
			{
				last_edge_index = num_of_vertices;
			}

			for (int j = parent_index; j < last_edge_index; j++)
			{
				if (adjacency_list[j].end_vertex == i)
				{
					reverse_edge_index = j;
					adjacency_list[j].reverse_edge_pos = edge_index;
					break;	
				}

				// Reverse edge will be allways found
			}			

			adjacency_list[edge_index].start_vertex = i;
			adjacency_list[edge_index].end_vertex = parent_index;			
			adjacency_list[edge_index].reverse_edge_pos = reverse_edge_index;
			adjacency_list[edge_index].next_edge_pos = -1;

			// Add next edge position to last added edge
			if (offspring_added)
			{
				adjacency_list[edge_index-1].next_edge_pos = edge_index;
			}

			edge_index++;
		}

		// Otherwise parent does not exist (e.g root)		
	}
}

// Make parts of Euler tour in parallel, adjust for root and send result to all processes 
void make_euler_tour(int proc_num, int *vertex_start_index, struct adjacency_list_element *adjacency_list, int *euler_tour)
{
	// Get next edge from reverse edge
	int next_edge_pos;
	int reverse_edge_index = adjacency_list[proc_num].reverse_edge_pos;
	if (adjacency_list[reverse_edge_index].next_edge_pos != -1)
	{
		next_edge_pos = adjacency_list[reverse_edge_index].next_edge_pos; 
	}
	else
	{
		// Otherwise next edge of euler tour is adjacent edge of end vertex
		int end_vertex = adjacency_list[proc_num].end_vertex;
		next_edge_pos = vertex_start_index[end_vertex];
	}

	// Adjust for root
	if (next_edge_pos == 0)
	{
		next_edge_pos = proc_num;
	}

	// Send value to all processes
	MPI_Allgather(&next_edge_pos, 1, MPI_INT, euler_tour, 1, MPI_INT, MPI_COMM_WORLD);
}

// Calculate ranks of edges using parallel list ranking
void get_ranks_of_edges(int proc_num, int num_of_edges, int *euler_tour, int *ranked_edges)
{
	// Initialize ranks
	int rank;
	int successor;
	int euler_tour_copy[num_of_edges];
	memcpy(euler_tour_copy, euler_tour, sizeof(int)*num_of_edges);

	if (euler_tour_copy[proc_num] == proc_num)
	{
		rank = 0;
	}
	else
	{
		rank = 1;	
	}

	// Distribute start values between processes
	MPI_Allgather(&rank, 1, MPI_INT, ranked_edges, 1, MPI_INT, MPI_COMM_WORLD);

	// Make path doubling for every edge
	// Result is edge position from last edge in Euler tour
	for (int k = 0; k < ceil(log2(num_of_edges)); k++)
	{
		rank = ranked_edges[proc_num] + ranked_edges[euler_tour_copy[proc_num]];
		successor = euler_tour_copy[euler_tour_copy[proc_num]];

		// Synchronize values between iterations
		MPI_Allgather(&successor, 1, MPI_INT, euler_tour_copy, 1, MPI_INT, MPI_COMM_WORLD);
		MPI_Allgather(&rank, 1, MPI_INT, ranked_edges, 1, MPI_INT, MPI_COMM_WORLD);
	}

	// Adjust position to by from first edge in Euler tour
	rank = num_of_edges - ranked_edges[proc_num];
	MPI_Allgather(&rank, 1, MPI_INT, ranked_edges, 1, MPI_INT, MPI_COMM_WORLD);
}

// Initialize weights for calculation of suffix sum
void initialize_weights(int proc_num, struct adjacency_list_element *adjacency_list, int *ranked_edges, int *suffix_sum_weights)
{
	// Initialize values in parallel
	bool forward_edge = ranked_edges[proc_num] < ranked_edges[adjacency_list[proc_num].reverse_edge_pos];
	int weight;

	if (forward_edge)
	{
		weight = 1;	
	}
	else
	{
		weight = 0;
	}

	// Distribute initial value
	MPI_Allgather(&weight, 1, MPI_INT, suffix_sum_weights, 1, MPI_INT, MPI_COMM_WORLD);
}

// Calculate suffix sum in parallel
void suffix_sum(int proc_num, int num_of_edges, int *euler_tour, int *suffix_sum_weights)
{
	// Initialize values
	MPI_Status status;
	int value;
	int successor;
	int last_value;
	int euler_tour_copy[num_of_edges];
	memcpy(euler_tour_copy, euler_tour, sizeof(int)*num_of_edges);

	if (euler_tour_copy[proc_num] == proc_num)
	{
		value = 0;
		last_value = suffix_sum_weights[proc_num];

		// Send last value to all other processes
		for (int i = 0; i < num_of_edges; i++)
		{
			if (i != proc_num)
			{
				MPI_Send(&last_value, 1, MPI_INT, i, DEFAULT_TAG, MPI_COMM_WORLD);
			}
		}
	}
	else
	{
		// Recieve last value
		MPI_Recv(&last_value, 1, MPI_INT, MPI_ANY_SOURCE, DEFAULT_TAG, MPI_COMM_WORLD, &status);			

		value = suffix_sum_weights[proc_num];
	}

	// Distribute start values between processes
	MPI_Allgather(&value, 1, MPI_INT, suffix_sum_weights, 1, MPI_INT, MPI_COMM_WORLD);

	// Calculate every value of suffix sum using path doubling
	for (int k = 0; k < ceil(log2(num_of_edges)); k++)
	{
		value = suffix_sum_weights[proc_num] + suffix_sum_weights[euler_tour_copy[proc_num]];
		successor = euler_tour_copy[euler_tour_copy[proc_num]];

		// Synchronize values between iterations
		MPI_Allgather(&successor, 1, MPI_INT, euler_tour_copy, 1, MPI_INT, MPI_COMM_WORLD);
		MPI_Allgather(&value, 1, MPI_INT, suffix_sum_weights, 1, MPI_INT, MPI_COMM_WORLD);
	}

	// If last value is not 0 correct all values by last value
	if (last_value != 0)
	{
		value = suffix_sum_weights[proc_num] + last_value;
		MPI_Allgather(&value, 1, MPI_INT, suffix_sum_weights, 1, MPI_INT, MPI_COMM_WORLD);
	}
}

// Get rank of every vertex in preorder traversal
void get_preorder_traversal(int proc_num, int num_of_vertices, struct adjacency_list_element *adjacency_list, int *ranked_edges, int *suffix_sum_weights, int *preorder_indexes_gather, int *preorder_ranks_gather)
{
	// Store position and rank for geather operation
	bool forward_edge = ranked_edges[proc_num] < ranked_edges[adjacency_list[proc_num].reverse_edge_pos];
	int vertex_index = -1;
	int vertex_rank = -1;

	if (forward_edge)
	{
		// Get rank and vertex
		vertex_index = adjacency_list[proc_num].end_vertex;
		vertex_rank = num_of_vertices - suffix_sum_weights[proc_num];
	}
	
	// Send values to other processes
	MPI_Allgather(&vertex_index, 1, MPI_INT, preorder_indexes_gather, 1, MPI_INT, MPI_COMM_WORLD);
	MPI_Allgather(&vertex_rank, 1, MPI_INT, preorder_ranks_gather, 1, MPI_INT, MPI_COMM_WORLD);
}

// Print vertices in preorder traversal
void print_preorder_traversal(int num_of_vertices, int num_of_edges, string binary_tree, int *preorder_indexes_gather, int *preorder_ranks_gather)
{
	// Construct output string from gather arrays
	char result_string[num_of_vertices + 1] = {0};

	// Add root to first position
	result_string[0] = binary_tree[0];

	// Add remaining values
	for(int i = 0; i < num_of_edges; i++)
	{
		// Check for valid value
		if (preorder_indexes_gather[i] != -1)
		{
			result_string[preorder_ranks_gather[i]] = binary_tree.at(preorder_indexes_gather[i]);
		}
	}

	cout << result_string << endl;
}

// ########################### Main ###########################
int main(int argc, char **argv)
{
	// Start processes
	MPI_Init(&argc, &argv);

	// Create variables for each process
	int proc_num;
	int num_of_proc;

	// Get number of each process and total number of processes
	MPI_Comm_rank(MPI_COMM_WORLD, &proc_num);
	MPI_Comm_size(MPI_COMM_WORLD, &num_of_proc);

	// Check arguments with first process
	if (proc_num == 0)
	{
		// Program needs only one character which contains binary tree
		if (argc != 2)
		{
			cout << "Program is used with one argument 'input_binary_tree'" << endl << endl << "- Binary tree is stored in string which contains its vertices. Vertices are numbered starting from 1 and every vertex is represented exactly by one character in string. Left child of vertex 'i' is stored on position '2*i' and right child on position '(2*i)+1'." << endl;
			return 1;
		}
	}

	// Dont run algorithms if input contains only 1 vertex
	string binary_tree_string = argv[1];
	if (num_of_proc == 1)
	{
		cout << binary_tree_string << endl;
		MPI_Finalize();
		return 0; 
	}

	// Make adjacency list for every process
	int num_of_edges = num_of_proc;
	int num_of_vertices = binary_tree_string.size(); 
	int vertex_start_index[num_of_vertices];
	struct adjacency_list_element adjacency_list[num_of_edges];
	
	make_adjacency_list(num_of_vertices, num_of_edges, vertex_start_index, adjacency_list);
	
 	#if DEBUG == true
	if (proc_num == 0)
	{
		cout << "Vertex start index:" << endl;
		for (int i = 0; i < num_of_vertices; i++)
		{
			cout << vertex_start_index[i] << endl;	
		}

		cout << "Adjacency list:" << endl;
		for (int i = 0; i < sizeof(adjacency_list)/sizeof(struct adjacency_list_element); i++)
		{
			cout << "[" << adjacency_list[i].start_vertex << "," << adjacency_list[i].end_vertex << "," << adjacency_list[i].reverse_edge_pos << "," << adjacency_list[i].next_edge_pos << "]" << endl; 
		}
	}
	#endif

	// Make Euler tour in parallel and adjust it for root
	int euler_tour[num_of_edges];
	make_euler_tour(proc_num, vertex_start_index, adjacency_list, euler_tour);

	#if DEBUG == true
	if (proc_num == 0)
	{
		cout << "Euler tour:" << endl;
		for (int i = 0; i < num_of_edges; i++)
		{
			cout << euler_tour[i] << endl; 
		}
	}
	#endif
	
	// Calculate position of edge in Euler tour with parallel list ranking
	int ranked_edges[num_of_edges];
	get_ranks_of_edges(proc_num, num_of_edges, euler_tour, ranked_edges);

	#if DEBUG == true
	if (proc_num == 0)
	{
		cout << "Edge possition:" << endl;
		for (int i = 0; i < num_of_edges; i++)
		{
			cout << ranked_edges[i] << endl; 
		}
	}
	#endif

	// Initialize weights for suffix sum
	int suffix_sum_weights[num_of_edges];
	initialize_weights(proc_num, adjacency_list, ranked_edges, suffix_sum_weights);

	#if DEBUG == true
	if (proc_num == 0)
	{
		cout << "Initial weights:" << endl;
		for (int i = 0; i < num_of_edges; i++)
		{
			cout << suffix_sum_weights[i] << endl;
		}
	}
	#endif

	// Calculate suffix sum to get values for preorder traversal
	suffix_sum(proc_num, num_of_edges, euler_tour, suffix_sum_weights);
	
	#if DEBUG == true
	if (proc_num == 0)
	{
		cout << "Suffix sum:" << endl;
		for (int i = 0; i < num_of_edges; i++)
		{
			cout << suffix_sum_weights[i] << endl;
		}
	}
	#endif

	// Get preorder traversal from suffix sum
	int preorder_indexes_gather[num_of_edges];
	int preorder_ranks_gather[num_of_edges];
	get_preorder_traversal(proc_num, num_of_vertices, adjacency_list, ranked_edges, suffix_sum_weights, preorder_indexes_gather, preorder_ranks_gather);

	#if DEBUG == true
	if (proc_num == 0)
	{
		cout << "Preorder vertex index and rank:" << endl;
		for (int i = 0; i < num_of_edges; i++)
		{
			cout << preorder_indexes_gather[i] << "-" << preorder_ranks_gather[i] << endl;
		}
	}
	#endif

	// Print vertices in preorder traversal with first process
	if (proc_num == 0)
	{
		print_preorder_traversal(num_of_vertices, num_of_edges, binary_tree_string, preorder_indexes_gather, preorder_ranks_gather);
	}
	
	// Stop processes and end program
	MPI_Finalize();
	return 0;
}

// END pro.cpp
